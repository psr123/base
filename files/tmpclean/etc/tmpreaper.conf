# Configuration for tmpreaper.  -*- conf -*-
#
# Supported configuration variables, from tmpreaper 1.6.13+nmu1:
#
# TMPREAPER_TIME
#       is the max. age of files before they're removed.
#       default:
#       the TMPTIME value in /etc/default/rcS if it's there, else
#       TMPREAPER_TIME=7d (for 7 days)
#       I recommend setting the value in /etc/default/rcS, as
#       that is used to clean out /tmp whenever the system is booted.
#
# TMPREAPER_PROTECT_EXTRA
#       are extra patterns that you may want to protect.
#       Example:
#       TMPREAPER_PROTECT_EXTRA='/tmp/isdnctrl* /tmp/important*'
#
# TMPREAPER_DIRS
#       are the directories to clean up.
#       *never* supply / here! That will wipe most of your system!
#       Example:
#       TMPREAPER_DIRS='/tmp/. /var/tmp/.'
#
# TMPREAPER_DELAY
#       defines the maximum (randomized) delay before starting processing.
#       See the manpage entry for --delay. Default is 256.
#       Example:
#       TMPREAPER_DELAY='256'
#
# TMPREAPER_ADDITIONALOPTIONS
#       extra options that are passed to tmpreaper, e.g. --all
#
# We leave TMPREAPER unset so that the default will be picked up from
# /etc/default/rcS as described above.

# This must not be set to true or tmpreaper won't run.
SHOWWARNING=false

# Use the defaults for most settings.
TMPREAPER_PROTECT_EXTRA=''
TMPREAPER_DIRS='/tmp/.'
TMPREAPER_DELAY=256

# We have several systems that generate a lot of noise in /tmp, which can
# cause tmpreaper to take a while to run.  Allow it 20 minutes, which seems to
# be long enough for any of our systems and is still safe against the race
# condition documented in the manual.
TMPREAPER_ADDITIONALOPTIONS='--runtime=1200'
