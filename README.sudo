This README file explains how to use base::sudo.

base::sudo can be used to enable and configure sudo for a system, with or 
without Duo.

base::sudo is a parameterized class.  If you include or require it without any 
parameters, the only thing that happens is, the sudo package is installed.  On 
Debian systems, that means anyone in the local "sudo" group has sudo access. On 
RHEL-family systems, that means anyone in the local "wheel" group has sudo 
access.  To set the sudo timeout (which defaults to 30 minutes), set the 
$timeout parameter.

WARNING: Without any parameters, this configuration means that, when you use 
sudo, you will be sending your password over the wire, and it will sit in 
memory on that remote system (which may be swapped to disk).

base::sudo also supports using Duo two-factor as the authentication method, 
instead of password.  To enable this, set the "duo" parameter to true, and set 
the "duo_sudoers" parameter to the list of people who will have this sudo 
access.

TIP: If you have a base::user class that defines your list of root users, you 
could simply pass that list into base::sudo.

WARNING: To be clear, setting the "duo" parameter to true will completely 
disable password-based authentication for sudo!

There are two additional Duo-related parameters that you should look at:

duo_fail_secure: If you set this to true, and the Duo service is 
unavailable or blocked, the sudo will fail.  Normally, the sudo would succeed.
This parameter does not apply if the Duo service is working: If you fail to 
answer a Duo call/push, or you deny the Duo call/push, then your sudo would 
still fail.

duo_gecos: If your local username on a system matches your username in Duo, 
then set this to false.  The default is to take your Duo username from the 
GECOS field of the passwd file (Puppet calls this field the "comment" field).
