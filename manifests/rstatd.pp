#
# Class to install and configure rstatd. This is only for load-balancing test.
# See portmap moudle. You need to open the portmap port to a specific machine.

class base::rstatd {
    include base::xinetd,
            base::portmap

    case $::osfamily {
        'RedHat': {
            package { 'rusers-server': ensure => present }

            file {
                '/etc/xinetd.d/rstatd':
                    source  => 'puppet:///modules/base/rstatd/etc/xinetd.d/rstatd',
                    require => [Package['rusers-server'], Service['portmap']],
                    notify  => Service['xinetd'];
                '/etc/xinetd.d/rstatd-udp':
                    source  => 'puppet:///modules/base/rstatd/etc/xinetd.d/rstatd-udp',
                    require => [Package['rusers-server'], Service['portmap']],
                    notify  => Service['xinetd'];
            }
        }
       'Debian': {
            # To be developed when needed.
       }
    }

    base::iptables::fragment { 'rstatd': ensure => present }
}

# Disable rstatd
class base::rstatd::disabled inherits base::rstatd {
    case $::osfamily {
        'RedHat': {
            File['/etc/xinetd.d/rstatd']     { ensure => absent }
            File['/etc/xinetd.d/rstatd-udp'] { ensure => absent }
        }
        'Debian': {
            # To be developed when needed.
        }
    }
    Base::Iptables::Fragment['rstatd'] { ensure => absent }
}
