# Provides libstdc++ libraries.  Debian normally doesn't need the help, since
# package dependencies should take care of this and we don't run as much
# unpackaged software on Debian, but we do still have to be able to install
# the version 5 library for Dell firmware updates.

class base::libstdc {
    if $osfamily == 'RedHat' {
        package {
            'libstdc++':       ensure => present;
            'libstdc++-devel': ensure => present;
        }
    }
}

class base::libstdc::v5 {
    case $osfamily {
        'Debian':           { package { 'libstdc++5': ensure => present } }
        'RedHat':           { include base::libstdc }
    }
}
