# Class: base::yumtools::plugin::versionlock
#
# This class installs versionlock plugin
#
# Parameters:
#   [*ensure*] - specifies if versionlock should be present or absent
#
# Actions:
#
# Requires:
#
# Sample usage:
#   include base::yumtools::plugin::versionlock
#
class base::yumtools::plugin::versionlock (
  $ensure = present
) {
  base::yumtools::plugin { 'versionlock':
    ensure  => $ensure,
  }
}
