#
# Define an xinetd service in a portable way between Red Hat and Debian.
# Currently, we require all xinetd configurations live in the xinetd module
# until we've finished the transition for Debian systems, but eventually
# modules will provide their own configuration files from their own files
# area.

define base::xinetd::service(
    $ensure,
    $source = 'NOSRC'
) {
    # Used for the file suffix.
    case $::osfamily {
        Debian: { $os = $::osfamily  }
        RedHat: { $os = "${::operatingsystem}${::lsbmajdistrelease}" }
    }

    case $ensure {
        present: {
            case $name {
                'eklogin', 'kshell', 'kshd': {
                    file { "/etc/xinetd.d/$name":
                        links  => follow,
                        source => $source ? {
                            NOSRC   => "puppet:///modules/base/xinetd/etc/xinetd.d/$name.$os",
                            default => $source
                        },
                        notify => Service['xinetd'],
                    }
                }
                default: {
                    file { "/etc/xinetd.d/$name":
                        source => $source ? {
                            NOSRC   => "puppet:///modules/base/xinetd/etc/xinetd.d/$name",
                            default => $source
                        },
                        notify => Service['xinetd'],
                    }
                }
            }
        }
        absent: {
            file { "/etc/xinetd.d/$name":
                ensure => absent,
                notify => Service['xinetd'],
            }
        }
    }
}
