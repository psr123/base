# RHEL6 requires a custom resolv.conf to deal with a single-threaded lookup
# bug which reduces performance to a crawl for services like sshd.
#
# TODO: remove EL6 custom resolv.conf when this bug is fixed.
define base::dns::resolv_conf (
  $ensure           = present,
  $dns_cache        = 'NONE',
  $first_dns_server = 'NONE',
  $is_dns_server    = false ,
) {
  if $::lsbdistcodename == 'santiago' {
    $set_dns_options = true
    $dns_options     = 'single-request-reopen'
  } else {
    $set_dns_options = false
  }

  if $dns_cache != 'NONE' {
    $set_dns_cache = true
  } else {
    $set_dns_cache = false
  }

  if $first_dns_server != 'NONE' {
    $set_first_dns_server = true
  } else {
    $set_first_dns_server = false
  }

  # resolv.conf is constructed from a template
  if  $is_dns_server  {
    $dns_server_name=$::hostname
  }

  file { '/etc/resolv.conf':
    ensure  => $ensure,
    content => template('base/dns/etc/resolv.conf.erb'),
  }
}
