# Manages NTP-related configugration and the ntpd service.

class base::ntp {
  package { 'ntp': ensure => present }

  # Handle transitions back from base::ntp::cron.
  file { '/etc/cron.d/ntpdate-loop':
    ensure => absent;
  }

  # Configuration files that are the same regardless of operating system.
  file {
    '/etc/filter-syslog/ntp':
      source => 'puppet:///modules/base/ntp/etc/filter-syslog/ntp';
    '/etc/ntp.conf':
      source => 'puppet:///modules/base/ntp/etc/ntp.conf',
      notify => Service['ntpd'];
  }

  # Operating-system-specific configuration.  In Debian, ntpdate is a
  # separate package.  In Red Hat, ntpdate comes with ntp package.  Also,
  # setup service according to OS.
  case $::osfamily {
    'RedHat': {
      service { 'ntpd':
        ensure  => running,
        require => Package['ntp'],
        enable  => true,
      }
      file {
        '/etc/ntp/ntpservers':
          source => 'puppet:///modules/base/ntp/etc/ntp/ntpservers',
          notify => Service['ntpd'];
        '/etc/sysconfig/ntpd':
          source => $::lsbdistrelease ? {
            3       => 'puppet:///modules/base/ntp/etc/sysconfig/ntpd.EL3',
            default => 'puppet:///modules/base/ntp/etc/sysconfig/ntpd.EL4',
          },
          notify => Service['ntpd'],
      }
      # On RHEL 8, we expect that systemd-timesync will be present, and not 
      # masked by NTP's installation.  So, disbale the service.
      if ( $::lsbmajdistrelease == 8 ) {
        service { 'systemd-timesyncd':
          ensure => stopped,
          enable => false,
        }
      }
    }
    'Debian': {
      package { 'ntpdate': ensure => present }
      service { 'ntpd':
        name      => $::lsbdistcodename ? {
          'sarge' => 'ntp-server',
          default => 'ntp'
        },
        ensure    => running,
        enable    => true,
        hasstatus => false,
        status    => 'pidof ntpd',
        require   => [
                      Package['ntp'],
                      Package['ntpdate'],
                     ]
      }
      file { '/etc/default/ntpdate':
        source => 'puppet:///modules/base/ntp/etc/default/ntpdate',
        notify => Service['ntpd'],
      }
    }
  }
}

# Required if the Nagios servers need to query ntpd.
# FIXME: Roll into the main class once we have a dev environment.
class base::ntp::nagios inherits base::ntp {
  File['/etc/ntp.conf'] {
    source => 'puppet:///modules/base/ntp/etc/ntp.conf.nagios',
  }
}

# If you don't want ntpd running, use this class.
class base::ntp::disabled inherits base::ntp {
  Service['ntpd'] {
    ensure => stopped,
    enable => false,
  }
}

# An alternative to ntpd is to run ntpdate in a cron job.
class base::ntp::cron inherits base::ntp::disabled {
  File['/etc/cron.d/ntpdate-loop'] {
    ensure => present,
    source => 'puppet:///modules/base/ntp/etc/cron.d/ntpdate-loop',
  }
}

# Used by systems that aren't at Stanford and hence don't have access to our
# time servers.
class base::ntp::off_campus inherits base::ntp {
  File['/etc/ntp.conf'] {
    source => 'puppet:///modules/base/ntp/etc/ntp.conf.off-campus',
  }

  # Some further adjustments are needed by operating system.
  case $::osfamily {
    'RedHat': {
      File['/etc/ntp/ntpservers'] {
        source => 'puppet:///modules/base/ntp/etc/ntp/ntpservers.off-campus',
      }
    }
    'Debian': {
      File['/etc/default/ntpdate'] {
        source => 'puppet:///modules/base/ntp/etc/default/ntpdate.off-campus',
      }
    }
    default: {}
  }
}
