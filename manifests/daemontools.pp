# Install daemontools and configure it to monitor /service on system boot.

class base::daemontools {
    package { 'daemontools': ensure => present }

    # RHEL 6 uses upstart.  Starting from inittab doesn't work any more.
    case $::lsbdistcodename {
        'santiago': {
            file { '/etc/init/daemontools.conf':
                source => 'puppet:///modules/base/daemontools/etc/init/daemontools.conf',
                notify => Exec['reload daemontools'],
            }
            exec { 'reload daemontools':
                command     => '/sbin/initctl reload daemontools',
                refreshonly => true,
            }
        }
    }
}