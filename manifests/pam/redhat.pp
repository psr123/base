#
# Sets up basic PAM configuration for RedHat, separated out from the original
# kerberos configuration.

class base::pam::redhat {
    package {
        'pam-afs-session': ensure => present;
        'pam_krb5':        ensure => absent;
        'pam_krb5-SU':     ensure => present;
    }

    case $lsbdistrelease {
        '3','4': {
            file {
                '/etc/pam.d/system-auth':
                    source  => 'puppet:///modules/base/pam/etc/pam.d/system-auth',
                    require => [ Package['pam_krb5-SU'],
                                 Package['pam-afs-session'] ];
            }
        }
        # RHEL5 and beyond
        default: {
            file {
                '/etc/pam.d/system-auth':
                    ensure => link,
                    target => '/etc/pam.d/system-auth-ac';
                '/etc/pam.d/system-auth-ac':
                    source  => 'puppet:///modules/base/pam/etc/pam.d/system-auth-ac',
                    require => [ Package['pam_krb5-SU'],
                                 Package['pam-afs-session'] ];
            }
        }
    }
}

class base::pam::redhat::ldap inherits base::pam::redhat {
    # todo if we ever get anything that actually uses this
}