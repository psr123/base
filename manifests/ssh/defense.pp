# Similar to base::ssh::global but with protection against a brute force attack by
# blocking too-frequent connections from one source.  Also ignore failed login
# attempts.

# FIXME: Change parameters to something more meaningful and make variables
# used by the template match the parameter names.
class base::ssh::defense(
  $secs  = 300,
  $limit = 5
) inherits base::ssh::ignore_fail {
  $ssh_lock_secs  = $secs
  $ssh_lock_count = $limit

  base::iptables::fragment_template { 'ssh-defense':
    ensure  => present,
    content => template('base/iptables/ssh-defense.erb'),
  }
  file { '/etc/filter-syslog/ssh-defense':
    source => 'puppet:///modules/base/ssh/etc/filter-syslog/ssh-defense',
  }

  # Reduce the number of allowed auth failures to 2 to cut down on brute-force
  # password guessing.
  Base::Ssh::Config::Sshd['/etc/ssh/sshd_config'] {
    max_tries => 2,
  }
}
