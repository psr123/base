# Generate a newsyslog configuration file.  This definition has the following
# limitations:
#
#  * All of the rotated logs must be located in the same directory and must
#    have the same ownership and permissions.
#
#  * All logs will be rotated on local disk.  Optionally, they can also be
#    archived in AFS, but the AFS path will always end in %Y/%M/%d/%m.%n.
#
#  * Only one block can be generated per configuration file fragment.
#
#  * newsyslog log symbolic names can't contain periods (or any other
#    characters other than letters, numbers, and underscores), so the names of
#    the logs have to be unique before the first period.  The rest is chopped
#    off to make the log name.
#
#  * Only one restart action can be set.
#
# Examples:
#
#     # Rotate /var/log/messages on local disk, analyze, and save 30 days.
#     base::newsyslog::config { 'messages':
#       frequency => 'daily',
#       directory => '/var/log',
#       restart   => 'hup /var/run/syslogd.pid',
#       log_owner => 'root',
#       log_group => 'root',
#       log_mode  => 640,
#       logs      => [ 'messages' ],
#       analyze   => '/usr/bin/filter-syslog',
#       save_num  => '30',
#     }
#
#     # Rotate Apache logs and save a copy into AFS.
#     base::newsyslog::config { 'apache':
#       frequency => 'daily',
#       directory => '/var/log/apache2',
#       archive   => '/afs/ir/service/auth/logs/weblogin',
#       restart   => 'run /usr/sbin/apache2ctl graceful',
#       log_owner => 'root',
#       log_group => 'root',
#       log_mode  => 644,
#       logs      => [ 'access.log', 'error.log' ],
#     }
#
# frequency defines the newsyslog directory into which this fragment should be
# put.  It must be one of the values 'daily', 'weekly', or 'monthly'.
#
# directory is the directory where the logs are located.  Lots will always be
# rotated into a subdirectory named OLD of that directory.  Optionally, logs
# can also be saved into an archive directory; this is done by adding the
# archive attribute.  If archive is set, '%Y/%M/%d/%m.%n' will be appended to
# create the archive path.
#
# restart sets the restart option.  It should always start with either hup or
# run.
#
# logs is a list of the logs to rotate.  log_owner, log_group, and log_mode
# are used to set the new owner, group, and mode of the empty file left behind
# after the logs are rotated.
#
# If analyze is set, it should be a path to a command.  By default, all
# rotated logs are run through that command as an analyze action.  If only a
# subset of logs should be analyzed, analyze_logs can be set to a list of that
# subset.

define base::newsyslog::config(
  $logs,
  $ensure       = 'present',
  $frequency    = 'daily',
  $directory    = '/var/log',
  $archive      = '',
  $restart      = '',
  $stop         = '',
  $log_owner    = 'root',
  $log_group    = 'root',
  $log_mode     = '640',
  $analyze      = '',
  $analyze_logs = [],
  $save_num     = '15'
) {
  if $name !~ /^\w+$/ { fail ("'${name}' contains an invalid character.") }
  file { "/etc/newsyslog.${frequency}/${name}":
    ensure  => $ensure,
    content => template('base/newsyslog/config.erb'),
    require => Package['newsyslog'],
  }
}
