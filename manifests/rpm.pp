#
# RH specific classes that provide RPM repositories.

# base::rpm::yumrepo definition
#
# namevar must match source file name
define base::rpm::yumrepo ($source='base/rpm') {
  file { "/etc/yum.repos.d/${name}":
    source  => "puppet:///modules/${source}/${name}",
    require => [ Package['yum'], File['/etc/yum.repos.d'] ]
  }
}

# base::rpm::openafs - provides openafs yum config
class base::rpm::openafs {
  $openafsver = $lsbmajdistrelease ? {
    '7'     => '1.6.8',
    '4'     => '1.6.2',
    default => '1.6.5',
  }
  base::rpm::yumrepo { "openafs-${openafsver}-EL${::lsbmajdistrelease}.repo": }
}

# rpm class.  applied to every RHEL system via basenode (os module)
class base::rpm {
  
  case $::osfamily {
    'RedHat': {
      include base::rpm::openafs
      $redhatPackages = [ 'yum', 'yum-utils' ]
      package { $redhatPackages: ensure => present }

      file { '/etc/yum.repos.d':
        ensure  => directory,
        mode    => '0755',
        recurse => true,
        purge   => true,
      }

      # Apply to all RHEL releases
      base::rpm::yumrepo { "dag-EL${::lsbmajdistrelease}.repo": }
      base::rpm::yumrepo { "stanford-priv-EL${::lsbmajdistrelease}.repo": }
      base::rpm::yumrepo { "stanford-EL${::lsbmajdistrelease}.repo": }
      # puppetlabs repo now being mirrored on yum.stanford.edu
      file { '/etc/yum.repos.d/puppet-mirror.repo':
        ensure  => present,
        content => template('base/etc/yum.repos.d/puppetlabs-mirror.repo.erb'),
      }
      base::rpm::import {'puppetlabs-mirror-rpmkey':
        url       => 'http://yum.stanford.edu/RPM-GPG-KEY-puppetlabs',
        signature => 'gpg-pubkey-4bd6ec30-4ff1e4fa',
      }

      file {
        # newsyslog config to rotate /var/log/yum.log
        '/etc/newsyslog.daily/yum':
          source  => 'puppet:///modules/base/rpm/etc/newsyslog.daily/yum',
          require => Package['newsyslog'];
      }

      case $::lsbmajdistrelease {
        # RHEL4
        '4': {
          include base::up2date

          base::rpm::yumrepo { 'rhel4.repo': }

        }
        # RHEL5+
        default: {
          # EL6/7 includes this plugin by default 
          if ($::lsbmajdistrelease == '5') {
            package { 'yum-downloadonly': 
              ensure => installed,
            }
          }
          # RHEL systems need this plugin enabled, due to new licensing
          if ($::operatingsystem == 'RedHat') {
            # enable yum rhn plugin
            exec { 'yum rhn plugin':
              command => "perl -pe 's/enabled = 0/enabled = 1/' -i /etc/yum/pluginconf.d/rhnplugin.conf",
              onlyif  => "[ -e /etc/yum/pluginconf.d/rhnplugin.conf ] && grep -q 'enabled = 0' /etc/yum/pluginconf.d/rhnplugin.conf",
            }
          } else {
            # disable yum rhn plugin
            exec { 'yum rhn plugin':
              command => "perl -pe 's/enabled = 1/enabled = 0/' -i /etc/yum/pluginconf.d/rhnplugin.conf",
              onlyif  => "[ -e /etc/yum/pluginconf.d/rhnplugin.conf ] && grep -q 'enabled = 1' /etc/yum/pluginconf.d/rhnplugin.conf",
            }
          }

          # enable yum plugins in general
          exec { 'enable yum plugins':
            command => "perl -pe 's/plugins=0/plugins=1/' -i /etc/yum.conf",
            onlyif  => "grep -q 'plugins=0' /etc/yum.conf",
            require => Exec['yum rhn plugin'],
          }
        }
      }

      # only RHEL4 and RHEL5 use rpmpkgs log
      if ($::lsbmajdistrelease == '4' or $::lsbmajdistrelease == '5') {
        # rotate /var/log/rpmpkgs weekly
        file { '/etc/newsyslog.weekly/rpmpkgs':
          source  => 'puppet:///modules/base/rpm/etc/newsyslog.weekly/rpmpkgs',
          require => Package['newsyslog'];
        }
      }
    }
    default: {
      warning('rpm.pp is being applied to a non-RHEL-based operating system')
    }
  }
}
