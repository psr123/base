# This code is from the Puppet Forge project justin8-systemd
#
# If you want to reload the systemd service on a unit file change, include
# this class and add a dependency:
#
#    notify => Exec['systemd-daemon-reload']
#

class base::systemd {

  exec { 'systemd-daemon-reload':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => 'systemctl daemon-reload',
    refreshonly => true,
  }

  case $::osfamily {
    'Archlinux': {
      $unit_path = '/usr/lib/systemd/system'
    }
    'Debian': {
      $unit_path = '/lib/systemd/system'
    }
    'RedHat': {
      $unit_path = '/usr/lib/systemd/system'
    }
    default: {
      $unit_path = '/usr/lib/systemd/system'
    }
  }

}
