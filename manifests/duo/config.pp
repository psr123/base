# Set up a custom Duo configuration. Note that this class does not _enable_ Duo.
# Instead, this type downloads a common Duo integration, copies it, and then 
# customizes it according to the parameters you specify.
#
# Your client code is responsible for leveraging the configuration, such as by 
# using PAM.
#
# See base::sudo and base::ssh for services that leverage this class.
#
# Here are the parameters:
#
# name: (namevar) The path to place the customized Duo configuration file.
#
# ensure: Set to "present" (the default) or "absent".
#
# wallet_name: the name for the common Duo wallet object. Defaults to the
# fully-qualified domain name of the host.
#
# use_gecos: A boolean, defaults to false.  When true, Duo will get the 
# username from the GECOS field (known in Puppet as the comment field) in the 
# system passwd file.  When false, Duo will use the user's username.  This is 
# used when a user is logging in with an account where their username does not 
# match their Duo username.
#
# fail_secure: A boolean, defaults to false.  When false, a Duo timeout will 
# cause the Duo authentication to pass, allowing the user to continue logging 
# in.  When true, a Duo timeout will cause the Duo authentication to fail, 
# blocking the user from logging in.

define base::duo::config (
  $ensure      = 'present',
  $wallet_name = $::fqdn,
  $use_gecos   = false,
  $fail_secure = false,
) {
  # Validate $ensure, $use_gecos, and $fail_secure
  if ($ensure != 'present') and ($ensure != 'absent') {
    fail('ensure may only be "present" or "absent"')
  }
  if !is_bool($use_gecos) {
    fail('base::duo::use_gecos must be true or false')
  }
  if !is_bool($fail_secure) {
    fail('base::duo::fail_secure must be true or false')
  }

  # Translate $use_gecos from boolean into present/absent
  if $use_gecos {
    $include_gecos_line = present
  } else {
    $include_gecos_line = absent
  }

  # Translate $fail_secure from boolean into present/absent
  if $fail_secure {
    $include_fail_line = present
  } else {
    $include_fail_line = absent
  }

  # Decide if we even need to do anything!
  if $ensure == 'present' {
    # If the common config hasn't been loaded, do that now
    if !defined(Base::Duo::Config::Common[$wallet_name]) {
      base::duo::config::common { $wallet_name:
        ensure => present,
      }
    }

    # First, copy our common Duo config file
    file { $name:
      ensure  => present,
      source  => "/etc/security/pam_duo_${wallet_name}.conf",
      replace => false,
      require => Base::Duo::Config::Common[$wallet_name],
    }

    # Next, add our GECOS and failmode lines
    file_line { "duo_gecos_${name}":
      ensure  => $include_gecos_line,
      path    => $name,
      line    => 'send_gecos = yes',
      require => File[$name],
    }
    file_line { "duo_fail_secure_${name}":
      ensure  => $include_fail_line,
      path    => $name,
      line    => 'failmode = secure',
      require => File[$name],
    }
  } # Done handling $ensure == 'present'

  else {
    # Make sure our custom Duo config file is gone
    file { $name:
      ensure => absent,
    }
  } # Done handling $ensure == 'absent'
}
