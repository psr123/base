# base::duo::package is used to bring in the packages that provide Duo 
# functionality.  This does not activate Duo, nor does it configure Duo.  
# Clients should `require base::duo::package` in their code, so that multiple 
# sources can "require" Duo.  Then, your code should declare an appropriate 
# base::duo::config to set up the configuration the way you want.
#
# As a convenience, if your code is using the base::duo::config type or the 
# base::duo::config::common type, then you get base::duo::package 
# automatically!

class base::duo::package (
  $wallet_name = $::fqdn,
  $use_gecos   = false,
  $fail_secure = false,
) {
  # What we pull in depends on the OS family
  case $::osfamily {
    redhat: {
      # For RHEL, bring in the duo_unix, with Duo's key
      include base::rpm::duo
      base::rpm::import { 'duo-rpmkey':
        url       => 'http://yum.stanford.edu/RPM-GPG-KEY-DUO',
        signature => 'gpg-pubkey-15d32efc-522883e4';
      }
      package { 'duo_unix':
        ensure => installed,
        require => Base::Rpm::Import[ 'duo-rpmkey' ],
      }
    }
    debian: {
      # The libpam-duo that ships with Debian doesn't support GECOS, so we need
      # a backport.  That means we need a pin, and THAT means we need to make
      # sure that the debian-stanford backports repository is already set up!
      # libpam-duo-gecos is a custom-made virtual package that ensures we are
      # using a new-enough version of libpam-duo.
      file { '/etc/apt/preferences.d/duo':
        ensure  => present,
        content => template('base/duo/duo.erb'),
        require => File['/etc/apt/sources.list.d/stanford.list'],
      }
      package { 'libpam-duo-gecos':
        ensure  => present,
        require => File['/etc/apt/preferences.d/duo'],
      }
    }
    default: {
      fail('Your OS family is not recognized for installing the Duo PAM module')
    }
  }
}
