#
# Rules specific to Oracle Linux systems. 
# Simply stubbing for now 
class base::os::oraclelinux {

    # For now, let's pretend it's just RedHat
    include base::os::redhat

}
