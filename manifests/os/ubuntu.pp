# General configuration for Ubuntu.  This is mostly the same as Debian, and
# hence inherits from it, but has some additional customization for Ubuntu.

# Supervise handling here should be moved to a daemontools module.
class base::os::ubuntu inherits base::os::debian {
  file {
    '/etc/apt/apt.conf.d/06apt':
      source => 'puppet:///modules/base/os/etc/apt/apt.conf.d/06apt';
    '/etc/bash.bashrc':
      source => 'puppet:///modules/base/os/etc/bash.bashrc';
  }

  # Triggered on Ubuntu to start the supervise process.  Only on 14.10 and below.
  if ($::lsbmajdistrelease < 15) {
    include base::daemontools
    file {
      '/etc/init/supervise.conf':
        source => 'puppet:///modules/base/os/etc/init/supervise.conf',
        notify => Exec['start supervise'],
    }
    exec { 'start supervise':
      command     => '/sbin/start supervise',
      refreshonly => true,
      require     => Package['daemontools'],
    }
  }
}
