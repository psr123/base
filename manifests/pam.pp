#
# Sets up basic PAM configuration, separated out from the original kerberos
# configuration.

# Basic class, for most systems.
class base::pam {
    case $osfamily {
        'RedHat': { include base::pam::redhat }
        'Debian': { include base::pam::debian }
    }
}

# Allows access to all users with a SUNet ID.  Mostly for timeshares and such.
class base::pam::ldap inherits base::pam {
    case $osfamily {
        'RedHat': { include base::pam::redhat::ldap }
        'Debian': { include base::pam::debian::ldap }
    }
}
