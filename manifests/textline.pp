
# Example of how to set an textline
#   base::textline { "kernel.sysreq = 0": ensure => "/etc/sysctl.conf"  }
#
# Example of how to ensure there is no textline
#   base::textline { "kernel.sysreq = 0": 
#       file => "/etc/sysctl.conf",
#       ensure => absent  
#   }
#
# If you are using any special characters in line, you will want to specify a
# escaped version so the existance of the line can be properly determined:
#   base::textline { "something=*": 
#       ensure => "/etc/somefile",
#       escaped_name => "something=\*"
#   }

define base::textline($ensure, $file="/tmp/nothing", $escaped_name="BASIC_REGEX") {
    $fn = $ensure ? {
        absent => $file,
        default => $ensure
    }

    $selector_pattern = $escaped_name ? {
        BASIC_REGEX => "^${name}$",
        default => $escaped_name
    }

    case $ensure {
        absent: {
            exec { "rm-txt-${fn}-${name}":
                command => "sed -i -e '/${selector_pattern}/d' ${fn}",
                onlyif => "grep '^[^#]' ${fn} | grep ${selector_pattern}"
            }
        }
        default: {
            $line = $name
            exec { "add-txt-${fn}-${name}":
                command => "echo '${line}' >> ${fn}",
                unless => "grep '${selector_pattern}' ${fn}"
            }
            exec { "fix-txt-${fn}-${name}":
                command => "sed -i -e '/${selector_pattern}/d' ${fn}; echo '${line}' >> ${fn}",
                unless => "grep '${selector_pattern}' ${fn}",
                require => Exec["add-txt-${fn}-${name}"]
            }
        }
    }
}