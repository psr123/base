#
# Manage grub.  Currently, we fix an error in the config file that keeps the
# timeout for console from working correctly on Red Hat systems.

class base::grub {
    $file = '/boot/grub/grub.conf'

    case $osfamily {
        'RedHat': {
            exec { 'cleangrubconf':
                command => "sed -i -e 's/--timeout/-timeout/' $file",
                onlyif  => "grep -- '--timeout' $file",
            }
        }
    }
}
