# Disable the effects of base::tmpclean.
#
# base::tmpclean is normally included from defaults.  This provides an easy
# way to override this if that particular system shouldn't clean out /tmp
# nightly by removing the daily cron job that does the cleaning.

class base::tmpclean::disabled inherits base::tmpclean {
  case $::osfamily {
    'Debian': {
      file { '/etc/cron.daily/tmpreaper': ensure => absent }
    }
    'RedHat': {
      File['/etc/cron.daily/tmpwatch'] { ensure => absent }
    }
  }
}
