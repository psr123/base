# Add the Puppet Labs Debian repository to Apt

class base::puppetclient::puppetlabs_repo {

  # Make sure we can find the Puppet Labs debian package repository.
  include base::apt_key
  base::apt_key::key { 'puppetlabs': ensure => present }

  # Get the Puppet Labs sources.list
  file { '/etc/apt/sources.list.d/puppetlabs.list':
    source  => 'puppet:///modules/base/puppetclient/etc/apt/sources.list.d/puppetlabs.list',
    require => Base::Apt_Key::Key['puppetlabs'],
    notify  => Exec['aptitude update'];
  }

}
