# Puppet client running in our Puppet 3 development environment.
class base::puppetclient::dev inherits base::puppetclient {
  Base::Puppetclient::Config['/etc/puppet/puppet.conf'] {
    server    => 'puppetservice1-dev.stanford.edu',
    ca_server => 'puppetca-dev.stanford.edu',
  }
}

