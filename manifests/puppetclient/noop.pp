# Used by systems that want to run Puppet in no-op mode.  This class
# probably only supports Debian right now.
class base::puppetclient::noop inherits base::puppetclient {
  Base::Puppetclient::Config['/etc/puppet/puppet.conf'] { in_noop => true }
}
