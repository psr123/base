# Set up the out_of_date client, which reports package status to the server.

# $platform: should be one 'production' or 'development'.

class base::out_of_date::client (
  $ensure              = 'present',
  $platform            = 'production',
  $ood_server_override = undef,
){
  if ($ensure == 'present') {
    # PRESENT

    # Determine the server to use.
    if ($ood_server_override) {
      $ood_server = $ood_server_override
    } else {
      case $platform {
        'production':  { $ood_server = 'frankoz2.stanford.edu' }
        'development': { $ood_server = 'frankoz2.stanford.edu' }
        default:       { fail("unrecognized platform") }
      }
    }

    package { 'stanford-outofdate-client':
        ensure => installed,
    }

    file {
      '/etc/out-of-date':
        ensure  => directory,
        owner   => root,
        group   => root;
      '/etc/out-of-date/client.conf':
        content => template('base/out_of_date/etc/out-of-date/client.conf.erb'),
        require => Package['stanford-outofdate-client'];
    }

    file { '/etc/cron.hourly/out-of-date':
      source => 'puppet:///modules/base/out-of-date/etc/cron.hourly/out-of-date',
    }
  } else {
    # ABSENT

    package { 'stanford-outofdate-client':
      ensure => absent,
    }

    file { '/etc/cron.hourly/out-of-date':
      ensure => absent
    }

    file {
      '/etc/out-of-date':
        ensure  => absent;
      '/etc/out-of-date/client.conf':
        ensure  => absent;
    }
  }
}
