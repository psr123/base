# disable ood-client by removing its cron job.

# DEPRECATED!
#
# Use instead base::out_of_date::client with the parameter 'ensure' set to
# absent.

class base::out_of_date::client::disabled {
  #    File['/etc/cron.hourly/out-of-date'] {
  #      ensure => absent
  #  }

  class { 'base::out_of_date::client':
    ensure => absent,
  }
}
